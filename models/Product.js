
const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Product name is required"]
	},
	description:{
		type: String,
		required: [true, "Product description is required"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
	items: {
		type: Number,
		required: [true, "Number of Item is required"]
	},
	isActive: {
		type: Boolean,
		default : true
	},
	createdOn :{
		type: Date,
		default: new Date()
	},
	customer: [
		{
			userId:{
				type: String,
				required: [true, "UserId is required"]
			},
			orderOn:{
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);