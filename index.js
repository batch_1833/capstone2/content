// Modules
const express = require ("express");
const mongoose = require ("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

// Server
const app = express();
const port = 3000;

// Connect to our MongoDB
 mongoose.connect("mongodb+srv://admin:admin@coursebooking.qvnarat.mongodb.net/capstone-project?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Notification for success or fail
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Connected to cloud database"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Routes for our API
// localhost:4000/users
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Listens to Port
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})

